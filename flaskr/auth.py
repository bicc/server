"""Routes for user authentication."""
from flask import redirect, render_template, flash, Blueprint, request, url_for, make_response

from flask_login import login_required, current_user, login_user
from flask import current_app as app
from .models import db, User
from .import login_manager

@login_manager.user_loader
def load_user(user_id):
    """Check if user is logged-in on every page load.
        The current user can be accesses via 'current_user'. Regular object of class 'User'
    """
    if user_id is not None:
        return User.query.get(int(user_id))
    return None


@login_manager.unauthorized_handler
def unauthorized():
    """Redirect unauthorized users to Login page."""
    return make_response('You must be logged in to view that page.', 403)


# actually I think I shouldn't use this and just check for a token in the non-login-required sensor/id api
#@login_manager.request_loader
#def load_user_with_token(request):
#    api_token = request.headers.get('Authorization')
#    user = User.query.filter_by(name=request.json["User"].first())
#    print("API request with api_token " + api_token + " for user " + user)
#    if api_token and user:
#        if api_token == user.get_api_token():
#            return user
#    return None
        
