# Flask & Vue Setup

## Idea

For the Vue application to run on the Flask Server, Flask uses the built vue html to render as a template (in routes.py). So unfortunately you have to rebuild the Vue application, for changes to take place (no hot reloading). 

You can still use the Vue development server (Port 8080) parallel to the Flask server (Port 5000) but this is just for development and the backend routing won't work correctly. So only use the Vue dev server for frontend works with hot reload.

## Routing

Flask and Vue use independet routing. The Flask routes are for the API Calls, the Vue routes for thr Frontend. Both have catch-all routes which, ~~*right now*, just send you to the "main page", the "SensorHub"~~