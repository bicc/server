"""Logged-in page routes."""
from flask import Blueprint, render_template, redirect, url_for, Markup, request, make_response, jsonify, Response, send_from_directory

from flask_login import current_user
from flask import current_app as app
from .models import db, Sensor, User, Measurement
from flask_login import login_required, logout_user, login_user
import json
from datetime import datetime, date
import pytz
import dateutil.parser
from operator import itemgetter
from flask_mail import Message
from . import mail

epoch = datetime.utcfromtimestamp(0)


# TODO improvement https://blog.miguelgrinberg.com/post/customizing-the-flask-response-class
def get_def_response():
    return Response(status=200, headers={"Content-Type": "application/json"})


def unix_time_millis(dt):
    return (dt - epoch).total_seconds() * 1000.0


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    return render_template('index.html')

@app.route('/static/<path:path>')
def send_static(path):
    # return app.send_static_file('img/favicon.png')
    return send_from_directory('static', path)

@app.route("/api/register", methods=['POST'])
def register():
    """
      User register logic.
      try it out with curl --data "name=X&password=Y" 127.0.0.1:5000/register
    """
    req = request.json
    resp = get_def_response()
    body = {}
    if User.query.filter_by(name=req["name"]).first() is None:
        try:
            user = User(name=req["name"])
            user.set_password(req["password"])
            db.session.add(user)
            db.session.commit()
            login_user(user)
            body["user-name"] = user.name
        except Exception as e:
            body["error"] = "Internal error"
            resp.status_code = 500
            print(e)
    else:
        body["error"] = "User already exists"
        resp.status_code = 400
    resp.set_data(json.dumps(body))
    return resp


@app.route("/api/login", methods=['POST'])
def login():
    req = request.json
    resp = get_def_response()
    password = req["password"]
    name = req["name"]
    user = User.query.filter_by(name=name).first()
    body = {}
    if user and user.check_password(password=password):
        login_user(user)
        body["user-name"] = user.name
    else:
        resp.status_code = 400
        body["error"] = "Username or password wrong"
    resp.set_data(json.dumps(body))
    return resp


@app.route("/api/logout", methods=['POST'])
@login_required
def logout():
    logout_user()
    print("logged out ")
    return get_def_response()


# currently not necessary
#@app.route("/user", methods=['GET', 'POST'])
#@login_required
#def user():
#    if request.method == 'POST':
#        current_user.create_api_token()
#    api_token = current_user.api_token
#    headers = get_def_header()
#    body = {"api-token" :  api_token,
#            "user-name" : current_user.name}
#    if api_token:
#        code = 200
#    else:
#        code = 400
#    return make_response(body,code,headers)


@app.route("/api/sensors", methods=['GET', 'POST'])
@login_required
def sensors():
    """
      curl --data "name=X&unit=Y" 127.0.0.1:5000/sensors
    """
    resp = get_def_response()
    body = {}
    if request.method == 'POST':
        try:
            req = request.json
            request_body = req["body"]
            sensor = Sensor(name=request_body["name"],
                            unit=request_body["unit"],
                            user_id=current_user.get_id(),
                            chart_type=request_body["chartType"],
                            notification_rule=0)
            sensor.create_api_token()
            db.session.add(sensor)
            db.session.commit()
            # TODO maybe Sensor class can always be represented as a JSON
        except Exception as e:
            body["error"] = "Internal error"
            resp.status_code = 500
            resp.set_data(json.dumps(body))
            print(e)
            return resp
    sensors = Sensor.query.filter_by(user_id=current_user.get_id()).order_by(
        Sensor.id).all()
    body = {"sensors": []}
    for sensor in sensors:
        json_sensor = {}
        json_sensor["name"] = sensor.name
        json_sensor["id"] = sensor.id
        json_sensor["apiToken"] = sensor.api_token
        json_sensor["chartType"] = sensor.chart_type
        json_sensor["unit"] = sensor.unit
        json_sensor["externalSource"] = sensor.external_source
        json_sensor["subscribers"] = []
        json_sensor["publisher"] = sensor.publisher
        json_sensor["notificationRule"] = sensor.notification_rule
        if sensor.notification_threshold:
            json_sensor[
                "notificationThreshold"] = sensor.notification_threshold
        if sensor.notification_address:
            json_sensor["notificationAddress"] = sensor.notification_address
        if sensor.subscribers:
            for subscriber in sensor.subscribers:
                json_subscriber = {}
                json_subscriber["id"] = subscriber.id
                json_subscriber["userId"] = subscriber.user_id
                json_subscriber["userName"] = User.query.filter_by(
                    id=subscriber.user_id).first().name
                json_sensor["subscribers"].append(json_subscriber)
        print(json_sensor)
        body["sensors"].append(json_sensor)
    resp.set_data(json.dumps(body))
    return resp


@app.route("/api/sensors/<int:sensor_id>", methods=['POST'])
def add_measurement(sensor_id):
    sensor = db.session.query(Sensor).filter_by(id=sensor_id).first()
    req = request.json
    print(req)
    resp = get_def_response()
    body = {}
    if sensor:
        if request.method == 'POST':
            request_body = req["body"]

            if request_body["token"] == sensor.api_token and sensor.api_token:
                # api-token needs to exist fit the provided token. Empty token occurs on subscriber sensors where it is not allowed to add a measurement to via api
                timestamp = request_body[
                    "timestamp"] if "timestamp" in request_body else datetime.now(pytz.timezone('Europe/Berlin')
                    )
                measurement = Measurement(value=float(request_body["value"]),
                                          timestamp=timestamp)

                for subscriber in sensor.subscribers:
                    copied_measurement = Measurement(
                        value=request_body["value"], timestamp=timestamp)
                    subscriber.measurements.append(copied_measurement)
                sensor.measurements.append(measurement)

                if ((sensor.notification_rule == 4) or
                    (sensor.notification_rule == 3
                     and measurement.value > sensor.notification_threshold) or
                    (sensor.notification_rule == 2
                     and measurement.value < sensor.notification_threshold)
                        or (sensor.notification_rule == 1 and measurement.value
                            == sensor.notification_threshold)):  # 2:<
                    message = Message("Sensorhub notification",
                                      sender="sensorhub.ubq@gmail.com",
                                      recipients=[sensor.notification_address])
                    message.body = "Your sensor " + sensor.name + " measured " + str(
                        measurement.value) + " " + sensor.unit
                    mail.send(message)
                try:
                    db.session.commit()
                except Exception as e:
                    print(e)
                    resp.status_code = 500
            else:
                resp.status_code = 403
                body["error"] = "Wrong API-Token!"
                resp.set_data(json.dumps(body))
                return resp
    else:
        resp.status_code = 404
        body["error"] = "Sensor with this ID does not exist!"
        resp.set_data(json.dumps(body))
    return resp


@app.route("/api/sensors/<int:sensor_id>", methods=['GET', 'PUT', 'DELETE'])
@login_required
def modify_sensor(sensor_id):
    """
      curl --data "value=X" 127.0.0.1:5000/register
      curl --data "value=X&timestamp=Y" 127.0.0.1:5000/register

      I haven't thought about which timestamps we want or work 
    """
    sensor = db.session.query(Sensor).filter_by(id=sensor_id).first()
    req = request.json
    print("REQ:", req)
    print("In put request")
    resp = get_def_response()
    body = {}
    if sensor:
        if request.method == 'PUT':
            request_body = req["body"]
            if "name" in request_body:
                sensor.name = request_body["name"]
            if "unit" in request_body:
                sensor.unit = request_body["unit"]
            if "chartType" in request_body:
                sensor.chart_type = request_body["chartType"]
            if "externalSource" in request_body:
                sensor.external_source = request_body["externalSource"]
            if "notificationRule" in request_body:
                rule = request_body["notificationRule"]
                if rule == 0:  # Never notify
                    sensor.notification_rule = request_body["notificationRule"]
                elif rule > 0 and rule < 4:  # 1:=   2:<    3:>    4:always
                    if "notificationThreshold" in request_body and "notificationAddress" in request_body:
                        sensor.notification_threshold = request_body[
                            "notificationThreshold"]
                        sensor.notification_address = request_body[
                            "notificationAddress"]
                        sensor.notification_rule = request_body[
                            "notificationRule"]
                    else:
                        resp.status_code = 400
                        body[
                            "error"] = "Notifications require threshold and address"
                elif rule == 4:
                    sensor.notification_rule = request_body["notificationRule"]
                    sensor.notification_address = request_body[
                        "notificationAddress"]
                else:
                    resp.status_code = 400
                    body["error"] = "Invalid rule"

            if "new_subscriber_name" in request_body:
                name = request_body["new_subscriber_name"]
                publisher = User.query.filter_by(id=sensor.user_id).first()
                subscriber = User.query.filter_by(name=name).first()
                if subscriber:
                    new_subscriber_sensor = Sensor(
                        name=publisher.name + "'s " + sensor.name,
                        unit=sensor.unit,
                        chart_type=sensor.chart_type,
                        user_id=subscriber.get_id(),
                        external_source=sensor.external_source,
                        notification_rule=0)
                    measurements = Measurement.query.filter_by(sensor_id=sensor.id).all()
                    for measurement in measurements:
                        copied_measurement = Measurement(
                            value=measurement.value,
                            timestamp=measurement.timestamp)
                        new_subscriber_sensor.measurements.append(
                            copied_measurement)
                    new_subscriber_sensor.publisher = sensor.id
                    sensor.subscribers.append(new_subscriber_sensor)
                else:
                    resp.status_code = 400
                    body["error"] = "User to share with doesn't exist"
                    resp.set_data(json.dumps(body))
                    return resp
            if "deleted_subscriber_name" in request_body:
                name = request_body["deleted_subscriber_name"]
                user_id = User.query.filter_by(name=name).first().id
                for subscriber in sensor.subscribers:
                    if subscriber.user_id == user_id:
                        print(subscriber)
                        db.session.delete(subscriber)
                        #Sensor.query.filter_by(id=subscriber.id).delete()
            db.session.commit()
        elif request.method == 'DELETE':
            print("Deleting sensor")
            print(sensor)
            for subscriber in sensor.subscribers:
                print(subscriber)
                db.session.delete(subscriber)
            db.session.delete(sensor)
            db.session.commit()
        measurements = Measurement.query.filter(Measurement.sensor_id == sensor_id).all()
        body = {"measurements": []}
        for measurement in measurements:
            json_measurement = {}
            json_measurement["id"] = measurement.id
            json_measurement["value"] = measurement.value
            json_measurement["timestamp"] = unix_time_millis(
                measurement.timestamp)
            body["measurements"].append(json_measurement)
        body["measurements"] = sorted(body["measurements"], key=itemgetter('timestamp'))
        print(body)
        resp.set_data(json.dumps(body))
    else:
        resp.status_code = 404
        body["error"] = "Sensor with this ID does not exist!"
        resp.set_data(json.dumps(body))
    return resp
