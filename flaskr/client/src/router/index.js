import Vue from 'vue';
import VueRouter from 'vue-router';

import Table from '../components/Table.vue'
import Login from '../components/Login.vue';
import SensorHub from '../components/SensorHub.vue';
import store from '../store/index.js'

Vue.use(VueRouter);


const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {path: '*', redirect: '/', meta: {authRequired: false}},
    {
      path: '/',
      name: 'home',
      redirect: '/sensor-hub',
      meta: {
        authRequired: false,
        title: 'Home - SensorHub',
      }
    },
    {
      path: '/sensor-hub',
      name: 'sensor-hub',
      component: SensorHub,
      meta: {
        authRequired: true,
        title: 'Home - SensorHub',
      }
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        authRequired: false,
        title: 'Login - SensorHub',
      }
    },
  ],
});

router.beforeEach((to, from, next) => {
  store.commit('initialiseStore')
  if (to.matched.some(record => record.meta.authRequired)) {
    if (store.state.user.name == null) {
      next({path: '/login'});
    } else {
      next();
    }
  }
  else {
    next();
  }
});

const DEFAULT_TITLE = "SensorHub";

// update document title when switching between routes
router.afterEach((to, from) => {
  Vue.nextTick(() => {
    document.title = to.meta.title || DEFAULT_TITLE;
  });
});

export default router;