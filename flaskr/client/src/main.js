import 'semantic-ui-css/semantic.min.css';

import Highcharts from 'highcharts';
import HighchartsVue from 'highcharts-vue'
import exportDataHC from 'highcharts/modules/export-data';
import exportingHC from 'highcharts/modules/exporting';
import SuiVue from 'semantic-ui-vue';
import Vue from 'vue';
import VueClipboard from 'vue-clipboard2'
import VueTippy, { TippyComponent } from "vue-tippy";

import App from './App.vue';
import router from './router';
import store from './store';


exportingHC(Highcharts);
exportDataHC(Highcharts);


Vue.config.productionTip = false;
Vue.use(SuiVue);
Vue.use(VueClipboard);
Vue.use(HighchartsVue);
Vue.use(VueTippy);
Vue.component("tippy", TippyComponent);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');

// Subscribe to store updates
store.subscribe((mutation, state) => {
  // Store the state object as a JSON string
  sessionStorage.setItem('store', JSON.stringify(state));
});
