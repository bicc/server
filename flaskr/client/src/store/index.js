import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

// sessionStorage
// https://www.mikestreety.co.uk/blog/vue-js-using-localstorage-with-the-vuex-store

export default new Vuex.Store({
  state: {user: {name: null}},
  getters: {},
  mutations: {
    initialiseStore(state) {
      if (sessionStorage.getItem('store')) {
        this.replaceState(
            Object.assign(state, JSON.parse(sessionStorage.getItem('store'))));
      }
    },
    loginUser(state, payload) {
      state.user.name = payload
    },
    logoutUser(state) {
      state.user.name = null
    }
  },
  actions: {}
});
