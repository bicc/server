const path = require('path');
var MiniCssExtractPlugin = require('mini-css-extract-plugin')
// const ExtractTextPlugin = require("extract-text-webpack-plugin");

process.env.NODE_ENV === 'production' ?
module.exports = {

  // comment the following code block when using the dev server via 'npm run
  // serve'
  //
  lintOnSave: false,

  assetsDir: '../static',
  publicPath: '',
  publicPath: undefined,
  outputDir: path.resolve(__dirname, '../templates'),
  runtimeCompiler: undefined,
  productionSourceMap: undefined,
  parallel: require('os').cpus().length > 1,

  css: {
    extract: false,
    sourceMap: true
  },

  // configureWebpack:{

  //   module: {
  //     rules: [
  //       // ... other rules omitted
  //       {
  //         test: /\.css$/,
  //         use: [
  //           {
  //             loader: MiniCssExtractPlugin.loader,
  //             options: {
  //               hmr: false
  //             }
  //           }, 
  //           'css-loader']
  //       }
  //     ]
  //   },
  //   plugins: [
  //     // ... Vue Loader plugin omitted
  //     new MiniCssExtractPlugin({
  //       filename: 'style.css'
  //     })
  //   ]
  // }

  //
} : module.exports = {
  lintOnSave: false,
};
