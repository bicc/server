"""Initialize app."""
from flask import Flask
from flask_talisman import Talisman
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_cors import CORS
from flask_mail import Mail
db = SQLAlchemy()
login_manager = LoginManager()
mail = Mail()


def create_app():
    """Construct the core app object."""
    app = Flask(__name__, instance_relative_config=False)

    # Enable CORS
    CORS(app, resources={r'/*': {'origins': '*'}})

    # Application Configuration
    app.config.from_object('config.Config')

    csp = {
        'default-src': '\'self\' https://gitlab.com',
        'img-src': ['*', 'data:'],
        'font-src': ['*', "https://fonts.gstatic.com", "data:"],
        'style-src': '\'unsafe-inline\' \'self\' https://fonts.googleapis.com',
        'script-src': '\'unsafe-inline\' \'self\'',
    }
    talisman = Talisman(app, content_security_policy=csp)

    # Initialize Plugins
    db.init_app(app)
    login_manager.init_app(app)
    mail.init_app(app)

    with app.app_context():
        from . import routes
        from . import auth

        # Create Database Models
        db.create_all()

        return app


app = create_app()
