"""Database models."""
from . import db
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
import secrets


class Sensor(db.Model):
    """Sensor model"""

    __tablename__ = 'sensor'

    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(100), nullable=False, unique=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    unit = db.Column(db.String(100), unique=False)

    measurements = db.relationship('Measurement', backref='sensor', lazy=True)

    external_source = db.Column(db.String(100), unique=False, nullable=True)
    chart_type = db.Column(db.String(100), unique=False, nullable=False)

    notification_address = db.Column(db.String(100),
                                     unique=False,
                                     nullable=True)

    notification_rule = db.Column(db.Integer, unique=False, nullable=True)

    notification_threshold = db.Column(db.Float, unique=False, nullable=True)

    publisher = db.Column(db.Integer, db.ForeignKey('sensor.id'))

    subscribers = db.relationship("Sensor")

    #Columm(db.Integer, db.ForeignKey('sensor.id'))

    #subscribing_sensors = db.relationship('RelatedSensors', backref='sensor', lazy=True)

    api_token = db.Column(db.String(200),
                          primary_key=False,
                          unique=False,
                          nullable=True)

    def __repr__(self):
        return 'Sensor #{} {}'.format(self.id, self.name)

    def create_api_token(self):
        self.api_token = secrets.token_hex(32)


#class RelatedSensors(db.Model):
#    """ related sensors where many are filled with the idential values of one core sensor"""
#
#    __tablename__ = 'relatedSensors'
#
#    id = db.Column(db.Integer, primary_key=True)
#
#    publishing_sensor = db.Column(db.Integer,
#                     db.ForeignKey('sensor.id'))
#    subscribing_sensor = db.Column(db.Integer,
#                     db.ForeignKey('sensor.id'))


class Measurement(db.Model):
    """Measurements model"""

    __tablename__ = 'measurement'

    id = db.Column(db.Integer, primary_key=True)

    sensor_id = db.Column(db.Integer, db.ForeignKey('sensor.id'))

    value = db.Column(db.Float)

    timestamp = db.Column(db.DateTime)

    def __repr__(self):
        unit = Sensor.query.filter_by(id=self.sensor_id).first().unit
        return 'Measurement #{} {} {} at {}'.format(self.id, self.value, unit,
                                                    self.timestamp)


class User(UserMixin, db.Model):
    """User account model."""

    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False, unique=True)
    password = db.Column(db.String(200),
                         primary_key=False,
                         unique=False,
                         nullable=False)

    sensors = db.relationship('Sensor', backref='user', lazy=True)

    def set_password(self, password):
        """Create hashed password."""
        self.password = generate_password_hash(password, method='sha256')

    def check_password(self, password):
        """Check hashed password."""
        return check_password_hash(self.password, password)

    def __repr__(self):
        return 'User {}'.format(self.name)
